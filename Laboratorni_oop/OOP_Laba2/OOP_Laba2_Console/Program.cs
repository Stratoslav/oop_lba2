﻿using System;

class Program
{
    static void Main(string[] args)
    {
        double x, y, z;

        Console.WriteLine("Введіть значення x:");
        if (double.TryParse(Console.ReadLine(), out x))
        {
            Console.WriteLine("Введіть значення y:");
            if (double.TryParse(Console.ReadLine(), out y))
            {
                Console.WriteLine("Введіть значення z:");
                if (double.TryParse(Console.ReadLine(), out z))
                {
                   
                    double s = Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2)) / (Math.Exp(x) * Math.Abs(Math.Cos(x + y))) + Math.Pow(Math.E, Math.Abs(x - y)) * (Math.Pow(Math.Tan(z), 2) + 1);

                    Console.WriteLine($"Результат обчислення s = {s:F3}"); 
                }
                else
                {
                    Console.WriteLine("Помилка: Некоректне значення z.");
                }
            }
            else
            {
                Console.WriteLine("Помилка: Некоректне значення y.");
            }
        }
        else
        {
            Console.WriteLine("Помилка: Некоректне значення x.");
        }

        Console.ReadLine(); 
    }
}
