﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Обчислення коренів квадратного рівняння ax^2 + bx + c = 0");

        Console.Write("Введіть значення a: ");
        if (!double.TryParse(Console.ReadLine(), out double a))
        {
            Console.WriteLine("Помилка: Некоректне введення для a.");
            return;
        }

        Console.Write("Введіть значення b: ");
        if (!double.TryParse(Console.ReadLine(), out double b))
        {
            Console.WriteLine("Помилка: Некоректне введення для b.");
            return;
        }

        Console.Write("Введіть значення c: ");
        if (!double.TryParse(Console.ReadLine(), out double c))
        {
            Console.WriteLine("Помилка: Некоректне введення для c.");
            return;
        }

        double discriminant = b * b - 4 * a * c;

        if (discriminant > 0)
        {
            double root1 = (-b + Math.Sqrt(discriminant)) / (2 * a);
            double root2 = (-b - Math.Sqrt(discriminant)) / (2 * a);
            Console.WriteLine($"Два корені: x1 = {root1}, x2 = {root2}");
        }
        else if (discriminant == 0)
        {
            double root = -b / (2 * a);
            Console.WriteLine($"Один корінь: x = {root}");
        }
        else
        {
            Console.WriteLine("Рівняння не має коренів у множині дійсних чисел.");
        }
    }
}
