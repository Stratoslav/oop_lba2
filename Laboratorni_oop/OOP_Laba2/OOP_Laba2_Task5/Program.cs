﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Виберіть, яке значення обчислити:");
        Console.WriteLine("1 - Сума 1^n + 2^n/2 + ... k^n/k");
        Console.WriteLine("2 - Сума 1^k + 2^k + ... n^k");
        Console.WriteLine("3 - Сума 1/n + 2/n^2 + 3/n^3 + ... k/n^k");

        int choice;
        if (!int.TryParse(Console.ReadLine(), out choice))
        {
            Console.WriteLine("Помилка: Введення повинно бути цілим числом.");
            return;
        }

        int n, k;
        double result = 0;

        switch (choice)
        {
            case 1:
                Console.Write("Введіть значення n: ");
                if (!int.TryParse(Console.ReadLine(), out n))
                {
                    Console.WriteLine("Помилка: Введення повинно бути цілим числом.");
                    return;
                }

                Console.Write("Введіть значення k: ");
                if (!int.TryParse(Console.ReadLine(), out k))
                {
                    Console.WriteLine("Помилка: Введення повинно бути цілим числом.");
                    return;
                }

                for (int i = 1; i <= k; i++)
                {
                    result += Math.Pow(i, n) / i;
                }
                break;

            case 2:
                Console.Write("Введіть значення k: ");
                if (!int.TryParse(Console.ReadLine(), out k))
                {
                    Console.WriteLine("Помилка: Введення повинно бути цілим числом.");
                    return;
                }

                Console.Write("Введіть значення n: ");
                if (!int.TryParse(Console.ReadLine(), out n))
                {
                    Console.WriteLine("Помилка: Введення повинно бути цілим числом.");
                    return;
                }

                for (int i = 1; i <= n; i++)
                {
                    result += Math.Pow(i, k);
                }
                break;

            case 3:
                Console.Write("Введіть значення k: ");
                if (!int.TryParse(Console.ReadLine(), out k))
                {
                    Console.WriteLine("Помилка: Введення повинно бути цілим числом.");
                    return;
                }

                Console.Write("Введіть значення n: ");
                if (!int.TryParse(Console.ReadLine(), out n))
                {
                    Console.WriteLine("Помилка: Введення повинно бути цілим числом.");
                    return;
                }

                for (int i = 1; i <= k; i++)
                {
                    result += i / Math.Pow(n, i);
                }
                break;

            default:
                Console.WriteLine("Невірний вибір. Виберіть значення від 1 до 3.");
                return;
        }

        Console.WriteLine($"Результат: {result}");
    }
}
